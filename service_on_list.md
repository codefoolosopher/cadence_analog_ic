### Services required for running IC(in ntsysv)

1. NetworkManager
1. autofs
1. crond
1. haldaemon
1. iptables
1. kdump
1. messagebus
1. netfs
1. network
1. nfs
1. nfslock
1. rpcbind
1. rsyslog
1. spice-vdagentd
1. sshd
1. systat
1. vsftpd
1. xinetd
1. xrdp(if xrdp is configured)


