# IC_Server_Config

[![Hits](https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fgithub.com%2FTeam-Neighborhood%2FI-want-to-study-Data-Science%2F)](https://hits.seeyoufarm.com)
[![Header1](https://img.shields.io/badge/NPIT-Cadence__IC-yellow)](http://npit.co.kr)
[![Header2](https://img.shields.io/badge/NPEDU-OpenHardWare__ICEDU-lightgrey)](http://openhardware.co.kr)
[![Header3](https://img.shields.io/badge/Cadence-Support-blueviolet)](https://support.cadence.com)
## Synopsys
- 본 페이지는 나인플러스IT(주)의 `Cadence Analog IC Package`의 관리를 위한 것입니다. 
- 현재 페이지의 항목에 대한 문제점 지적이나 수정 및 증보 요청은 sawoo@npit.co.kr(02-6123X3349)로 문의 주시길 바랍니다.
- 본 Wiki Page는 계속 업데이트 예정이며, 관련 고객사(학교)는 주요 업데이트 내용이 있을 경우 공지 메일을 보내어 드립니다.

- EDA툴에 대한 전반적인 소개 및 환경은 IDEC에서 발간한 ["IDEC EDA Tool 소개자료"](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/blob/master/EDA_Introduction.pdf)를 참조하시길 바랍니다.


## Table of Contents::User Manual
- [OS 기본 설정관련]
    - [OS버전호환](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/blob/master/OS_Comaptibility_Table.pdf)
    - [OS 설치]
        - [RHEL6(CentOS6)]
        - [RHEL7(CentOS7)](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/Install_RHEL7)
    - [OS기본 설정]
        - [패키지 설치](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/Install_PKG)
        - [SELinux 해제](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/SELinux_Release)
        - [Hostname 설정](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/HostName)
        - [고정아이피설정](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/StaticIP)
        - [유저추가 기본설정(skel)](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/UserSkeleton)
        - [유저 기본 셸(SHELL) 설정](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/UserShell)
        - [SSH 설정](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/SSH)
            - [SELinux 설정시 SSH설정]()
    - [디스크 LVM관리](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/LVM_resize)
    - [백업(rsync/cron)]()
    - [협업 및 파일 공유]
        - [SFTP](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/Sftp)
        - [Samba](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/Samba) 
        - [SVN]()
    - [하드디스크 추가](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/Extension_HDD)
- [원격지원 설정(Server Side)]
    - [Anydesk on Linux 설치/설정](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/AnyDesk)
    - [TeamViewer on Linux 설치/설정](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/TeamViewer)
- [X11 터미널 프로그램 설정]
    - [Putty-Ming](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/Putty_Ming_Setting)
    - [mobaXterm](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/MobaXterm)
- [VNC 설정](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/vnc)
- [XRDP 설정](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/XRDP)
- [Analog IC 버전 호환]
	- [IC6.1.7](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/IC617_Compatibility_Table)
	- [IC6.1.8](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/IC618_Compatibility_Table)
- [IC6.1.X 설정관련]
- [Mentor Calibre 설정관련](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/Calibre)
- [라이센스 관리](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/Cadence-License-Maintenance)
- [C-Shell 스크립트 관리](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/C-Shell-Script-Configuration)
- SKILL 개발환경 설정
    - [Visual Studio Code](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/Vscode)
    - [Sublime Text3](https://gitlab.com/codefoolosopher/cadence_analog_ic/-/wikis/Sublime)