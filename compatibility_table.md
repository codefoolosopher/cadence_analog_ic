-----------------------------------------------------------------------------------------
Cadence Product Release Compatible with IC6.1.7
-----------------------------------------------------------------------------------------
Spectre Cirtuit Simulators............................(*SPECTRE 17.10.458)
Pegasus/Physical Verification System..................(PEGASUS 18.30.000/PVS 16.12.007)
Assura Physical Verification..........................(**ASSURA 04.15.116)
XCELIUM...............................................(XCELIUMMAIN 18.03.013)
Conformal.............................................(CONFRML 18.10.300)
Innovus...............................................(INNOVUS 18.11.000)
Manufactuability and Variability Sign-off.............(MVS 18.11.000)
Extraction Tools (QRC/Quantus QRC)....................(EXT 18.12.000)
Allegro Sigrity.......................................(SIG 17.00.012)
Silicon-Package-Board Co-Design.......................(SPB 17.20.048)

* SEPCTRE171
1.Base_SPECTRE17.10.124_lnx86
2.Hotfix_SPECTRE17.10.627_lnx86

* ASSURA416
1.Update_ASSURA04.16.001(ASSURA_41USR6_OA-617)
2.Hotfix_ASSURA04.16.106(ASSURA_41USR6HF6_OA-617)



-----------------------------------------------------------------------------------------
Cadence Product Release Compatible with IC6.1.8
-----------------------------------------------------------------------------------------
Spectre Circuit Simulators............................(SPECTRE 18.10.370)
Pegasus/Physical Verification System..................(PEGASUS 19.10.000)
Physical Verification System..........................(PVS 16.14.000)
Assura Physical Verification..........................(ASSURA 04.16.102)
XCELIUM...............................................(XCELIUMMAIN 19.03.006)
Conformal.............................................(CONFRML 19.10.100)
Innovus...............................................(INNOVUS 19.10.000)
Extraction Tools (QRC/Quantus QRC)....................(EXT 19.12.000)
Allegro Sigrity.......................................(SIG 18.00.004)
Silicon-Package-Board Co-Design.......................(SPB 17.20.055)


