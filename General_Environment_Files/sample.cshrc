#####################################################################
# .cshrc environmet for EDA Tools
# Author: NinePlusIT Co. Ltd.,(2020)
# E-Mail: sawoo@npit.co.kr
# Last Modified: 02-Feb-2020
#####################################################################

#####################################################################
# General Environment Configuration				    #
#####################################################################
set SHELL = /bin/csh
umask 022		 # Create new files with 755 permission
limit coredumpsize 50000 # Create core dump file limited 50000 bytes
setenv ISCAPE_SLEEP 2
#####################################################################
# License Control						    #
#####################################################################
setenv CDS_LIC_FILE 5280@CDS_LIC_SITE # defined in /etc/hosts
setenv LM_LICNESE_FILE $CDS_LIC_FILE

#####################################################################
# Common PATH Configuration					    #
#####################################################################
setenv CDS_ROOT /eda_tools/cadence
#setenv CDS_SITE /smb # for CIS/CIP

#####################################################################
# IC6.1.7 Configuration						    #
#####################################################################
setenv SPECTREHOME /$CDS_SETUP/SPECTRE171
setenv PVSHOME /$CDS_SETUP/PVS161
setenv ASSURA_USE_PVS_LICENSE
setenv CDS_INST_DIR /$CDS_SETUP/IC617
setenv EXTHOME /$CDS_SETUP/EXT181
setenv QRC_ENABLE_EXTRACTION
setenv ASSURAHOME /$CDS_SETUP/ASSURA416
setenv LANG C
setenv SUBSTRATESTORHOME $ASSURAHOME
setenv CDSHOME $CDS_INST_DIR
setenv OA_HOME $CDSHOME/oa_v22.50.095
setenv CDS_Netlisting_Mode Analog
setenv CDS_AUTO_64BIT_ALL
setenv W3264_NO_HOST_CHECK 1

set path = ( $path $SPECTREHOME/tools/bin $SPECTREHOME/tools/dfII/bin )
set path = ( $path $PVSHOME/tools/bin $PVSHOME/tools/dfII/bin )
set path = ( $path $CDSHOME/tools/dfII/bin $CDSHOME/tools/bin )
set path = ( $path $EXTHOME/tools/bin $EXTHOME/tools/dfII/bin )
set path = ( $path $ASSURAHOME/tools/bin $ASSURAHOME/tools/dfII/bin )

#####################################################################
# User Defined Alias						    #
#####################################################################
alias iscape '/eda_tools/cadence/IS/iscape.04.23-s012/bin/iscape.sh'
