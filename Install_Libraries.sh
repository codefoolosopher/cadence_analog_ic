#!/bin/bash

# Script to install libraries for IC617 in a compliance with dependency.
# In order to see dependency of each library, run 
# {/HOME_EACH_TOOL}/checkSysConf -R .

# (!) This script should be implemented in a "root" user.

#Library Install
\yum install -y lsb
\yum install -y elfutils-libelf
\yum install -y ksh
\yum install -y openmotif22
\yum install -y openmotif22.i686
\yum install -y libpng
\yum install -y libjpeg
\yum install -y compat-expat1
\yum install -y expat-devel
\yum install -y expat
\yum install -y glibc-devel
\yum install -y glibc-devel.i686
\yum install -y xterm.x86_64
\yum install -y libstdc++.x86_64
\yum install -y glibc
\yum install -y glibc.i686
\yum install -y libSM.i686
\yum install -y libXrender.i686
\yum install -y libXt
\yum install -y libXt.i686
\yum install -y libXp
\yum install -y libXp.i686
\yum install -y libXext.i686
\yum install -y libXtst.i686
\yum install -y zlib.i686
\yum install -y mesa-libGL
\yum install -y mesa-libGL.i686
\yum install -y mesa-libGLU
\yum install -y mesa-libGLU.i686
\yum install -y xorg-x11-fonts-ISO8859-1-75dpi.noarch
\yum install -y baekmuk-ttf*

#yum update to remain anew
\yum -y update
